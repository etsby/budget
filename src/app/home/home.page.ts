import { Component, OnInit } from '@angular/core';
import { Task } from '../../task';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  inprogress = Array<Task>();
  todo = Array<Task>();
  finished = Array<Task>();

  constructor(public tasksServise: TasksService) {}

  ngOnInit() {
    this.inprogress = this.tasksServise.inprogress;
    this.todo = this.tasksServise.todo;
    this.finished = this.tasksServise.finished;
  }

  doReorder(ev: any) {
    console.log(ev);
    ev.detail.complete();
  }

}
